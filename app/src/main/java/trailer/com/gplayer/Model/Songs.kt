package trailer.com.gplayer.Model

import java.util.Objects

class Songs {

    var title: String? = null
    var artist: String? = null
    var album: String? = null
    var data: String? = null
    var duration: Long = 0

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is Songs) return false
        val that = o as Songs?
        return duration == that!!.duration &&
                title == that.title &&
                artist == that.artist &&
                album == that.album &&
                data == that.data
    }

    override fun hashCode(): Int {

        return Objects.hash(title, artist, album, data, duration)
    }

    override fun toString(): String {
        return "Songs{" +
                "title='" + title + '\''.toString() +
                ", artist='" + artist + '\''.toString() +
                ", album='" + album + '\''.toString() +
                ", data='" + data + '\''.toString() +
                ", duration=" + duration +
                '}'.toString()
    }
}
