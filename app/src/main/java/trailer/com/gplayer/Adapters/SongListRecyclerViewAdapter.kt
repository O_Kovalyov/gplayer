package trailer.com.gplayer.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import kotlinx.android.synthetic.main.song_item.view.*
import rx.Observable
import rx.subjects.PublishSubject
import trailer.com.gplayer.Comparator.SongComparator
import trailer.com.gplayer.Model.Songs
import trailer.com.gplayer.R
import kotlin.properties.Delegates

class SongListRecyclerViewAdapter : RecyclerView.Adapter<SongListRecyclerViewAdapter.SongListViewHolder>(), DiffUtilsAdapterInterface {
    var songList: List<Songs> by Delegates.observable(emptyList()) {
        prop, old, new ->
        autoNotify(old, new) { o, n -> (o.title + o.artist + o.album) == (n.title + n.artist + n.album) }
    }
    private val clickSubject = PublishSubject.create<Songs>()
    val clickEvent: Observable<Songs> = clickSubject

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.song_item, parent, false)
        return SongListViewHolder(view)
    }

    override fun onBindViewHolder(holder: SongListViewHolder, position: Int) {
        holder.songTitle?.text = songList[position].title
        holder.songArtist?.text = songList[position].artist
        holder.moreBtn?.setOnClickListener{ view -> }
    }

    inner class SongListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //val poster: ImageView? = itemView.songPoster
        val songTitle: TextView? = itemView.songTitle
        val songArtist: TextView? = itemView.songArtist
        val moreBtn: ImageButton? = itemView.songListMorebtn

        init {
            itemView.setOnClickListener {
                clickSubject.onNext(songList[layoutPosition])
            }
        }
    }

    override fun getItemCount(): Int = songList.size

    fun sortContent(sortTipe: Comparator<Songs>){
        songList.sortedWith(sortTipe)
    }
}
