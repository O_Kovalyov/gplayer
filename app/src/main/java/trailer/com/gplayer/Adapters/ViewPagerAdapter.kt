package trailer.com.gplayer.Adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import java.util.ArrayList
import java.util.Arrays

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    internal var fragments: MutableList<Fragment> = ArrayList()

    fun addScreens(vararg args: Fragment): List<Fragment> {
        fragments.addAll(Arrays.asList(*args))
        return fragments
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }
}
