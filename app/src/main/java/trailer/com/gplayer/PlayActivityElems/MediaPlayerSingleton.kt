package trailer.com.gplayer.PlayActivityElems

import android.media.AudioManager
import android.media.MediaPlayer

class MediaPlayerSingleton private constructor(){
    private var mPlayer: MediaPlayer
    init{
        mPlayer = MediaPlayer()
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
    }
    private object Holder {val INSTANSE = MediaPlayerSingleton()
    }

    companion object {
        val instance: MediaPlayerSingleton by lazy { Holder.INSTANSE }
    }
    fun getMPlayer():MediaPlayer = mPlayer
}