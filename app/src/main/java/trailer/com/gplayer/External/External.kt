package trailer.com.gplayer.External

import android.content.ContentResolver
import android.provider.MediaStore
import trailer.com.gplayer.Model.Songs

class External{

    fun getSongs(contentResolver: ContentResolver): ArrayList<Songs>{
        val audioList: ArrayList<Songs> = ArrayList()
        try {
            val proj = arrayOf(MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
                    MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.DURATION,
                    MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.DATA)
            val selection = MediaStore.Audio.Media.DURATION + " >= 50000"

            val audioCursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, proj,
                    selection, null, null)


            if (audioCursor != null) {
                if (audioCursor.moveToFirst()) {
                    do {
                        val audioTitle = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE)
                        val audioartist = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST)
                        val audioduration = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)
                        val audioalbum = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM)
                        val audiodata = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)
                        val info = Songs()
                        info.data = audioCursor.getString(audiodata)
                        info.title = audioCursor.getString(audioTitle)
                        info.duration = audioCursor.getLong(audioduration)
                        info.album = audioCursor.getString(audioalbum)
                        info.artist = audioCursor.getString(audioartist)
                        audioList.add(info)
                    } while (audioCursor.moveToNext())
                }
            }
            audioCursor!!.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return audioList
    }
}