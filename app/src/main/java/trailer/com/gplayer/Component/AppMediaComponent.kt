package trailer.com.gplayer.Component

import android.media.MediaPlayer
import dagger.Component
import trailer.com.gplayer.MediaModule.MediaPlayerModule
import trailer.com.gplayer.MediaModule.MediaPlayerScopeAnnotation
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(MediaPlayerModule::class))
interface AppMediaComponent {
    fun getMediaPlayerComponent(): MediaPlayer
}