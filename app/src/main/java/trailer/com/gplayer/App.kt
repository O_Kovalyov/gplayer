package trailer.com.gplayer

import android.app.Application
import android.media.MediaPlayer
import trailer.com.gplayer.Component.DaggerAppMediaComponent

class App: Application() {
    private val  mPlayer : MediaPlayer = DaggerAppMediaComponent.create().getMediaPlayerComponent()
    fun getMediaPlayer(): MediaPlayer {
        /*if (mPlayer!= null){
            return  mPlayer
        }*/
        return mPlayer
    }
}
