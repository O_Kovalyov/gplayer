package trailer.com.gplayer.MediaModule

import android.media.MediaPlayer
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MediaPlayerModule {
    @Provides
    @Singleton
    fun provideMediaPlayer():MediaPlayer = MediaPlayer()
}