package trailer.com.gplayer.MediaModule

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class MediaPlayerScopeAnnotation