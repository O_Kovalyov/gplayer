package trailer.com.gplayer.RxObservables

import android.opengl.Visibility
import android.view.View
import android.widget.ImageButton
import android.widget.SearchView
import kotlinx.android.synthetic.main.song_list.*
import rx.Observable
import rx.Observer
import rx.Subscriber
import rx.Subscription
import rx.subjects.PublishSubject

class RxSearchObservable {

    fun queryChangeHandlerFromView(searchView: android.support.v7.widget.SearchView): Observable<String> {
        var subject: PublishSubject<String> = PublishSubject.create()
        searchView.setOnQueryTextListener(object: android.support.v7.widget.SearchView.OnQueryTextListener{
            override fun onQueryTextChange(newText: String?): Boolean {
               subject.onNext(newText)
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                subject.onCompleted()
                return true
            }
        })
        return subject
    }

    fun closeHandlerFromView(searchView: android.support.v7.widget.SearchView, viewToHide: ImageButton): Observable<Boolean>{
        return Observable.create {
            searchView.setOnCloseListener { viewToHide.visibility = View.VISIBLE
                false}
            searchView.setOnSearchClickListener{_-> viewToHide.visibility = View.INVISIBLE}
        }
    }

}