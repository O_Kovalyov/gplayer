package trailer.com.gplayer.FragmentScreens

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import trailer.com.gplayer.R

class FragmentAlbum : Fragment() {

    private val page: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //page = getArguments().getInt("someInt", 0);
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_album_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    /*@Override
    public Fragment newInstanse(int position) {
        Fragment fragmentAlbum = new FragmentAlbum();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        fragmentAlbum.setArguments(args);
        return fragmentAlbum;
    }*/
}
