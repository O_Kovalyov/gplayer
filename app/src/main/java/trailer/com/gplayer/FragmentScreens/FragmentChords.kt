package trailer.com.gplayer.FragmentScreens

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import trailer.com.gplayer.R

class FragmentChords : Fragment() {

    private val page: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //page = getArguments().getInt("someInt", 0);
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chords_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    companion object {

        fun newInstance(page: Int): FragmentChords {
            val fragmentChords = FragmentChords()
            val args = Bundle()
            args.putInt("someInt", page)
            fragmentChords.arguments = args
            return fragmentChords
        }
    }
    /*
    public Fragment newInstanse(int position) {
        Fragment fragmentChords = new FragmentChords();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        fragmentChords.setArguments(args);
        return fragmentChords;
    }*/
}
