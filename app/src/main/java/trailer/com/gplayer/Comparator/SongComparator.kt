package trailer.com.gplayer.Comparator

import trailer.com.gplayer.Model.Songs
import java.util.Comparator

class SongComparator{
    private val TITLE_COMPARATOR = Comparator<Songs> { part1, part2 ->
        val parts1 = (part1.title!!.toLowerCase())
        val parts2 = (part2.title!!.toLowerCase() )
        parts1.compareTo(parts2)
    }
    private val ARTIST_COMPARATOR = Comparator<Songs> { part1, part2 ->
        val parts1 = (part1.artist!!.toLowerCase())
        val parts2 = (part2.artist!!.toLowerCase() )
        parts1.compareTo(parts2)
    }
    private val ALBUM_COMPARATOR = Comparator<Songs> { part1, part2 ->
        val parts1 = (part1.album!!.toLowerCase())
        val parts2 = (part2.album!!.toLowerCase() )
        parts1.compareTo(parts2)
    }
    fun filter(songList : List<Songs>, query : String): List<Songs>{
        if (query == null || query.isEmpty()) return songList
        val lowerCaseQuery : String = query.toLowerCase()
        val filteredSongList : ArrayList<Songs> = ArrayList()
        for(song in songList){
            val artistAndTitleAndAlbum = "${song.artist} ${song.title} ${song.album}"
            if(artistAndTitleAndAlbum.contains(lowerCaseQuery)){
                filteredSongList.add(song)
            }
        }
        return filteredSongList
    }

    fun getTitleComparator() : Comparator<Songs>{
        return TITLE_COMPARATOR
    }

    fun getArtistComparator() : Comparator<Songs>{
        return ARTIST_COMPARATOR
    }

    fun getAlbumComparator() : Comparator<Songs>{
        return ALBUM_COMPARATOR
    }

}