package trailer.com.gplayer

import android.media.MediaPlayer
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.View
import android.widget.SeekBar
import com.jakewharton.rxbinding.widget.RxSeekBar
import com.jakewharton.rxbinding.widget.SeekBarProgressChangeEvent
import kotlinx.android.synthetic.main.activity_play.*
import rx.android.schedulers.AndroidSchedulers
import trailer.com.gplayer.Adapters.ViewPagerAdapter
import trailer.com.gplayer.FragmentScreens.FragmentAlbum
import trailer.com.gplayer.FragmentScreens.FragmentChords
import trailer.com.gplayer.Model.Songs
import trailer.com.gplayer.PlayActivityElems.MediaPlayerSingleton
import trailer.com.gplayer.Presenters.PlayActivityPresenter
import trailer.com.gplayer.Presenters.PlayerView

class PlayActivityView : FragmentActivity(), View.OnClickListener, PlayerView {

    var presenter: PlayActivityPresenter? = null
    lateinit var seekBarUserSubscription: rx.Subscription
    private lateinit var mediaPlayer: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play)
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        viewPagerAdapter.addScreens(FragmentAlbum(), FragmentChords())
        viewPager?.adapter = viewPagerAdapter
        btnPlay.setOnClickListener { v: View? -> onClick(v) }
        btnPause.setOnClickListener { v: View? -> onClick(v) }
        btnNext.setOnClickListener { v: View? -> onClick(v) }
        btnPrevious.setOnClickListener { v: View? -> onClick(v) }
        mediaPlayer = MediaPlayerSingleton.instance.getMPlayer()
        presenter = PlayActivityPresenter(mediaPlayer, this)
        presenter?.getStoredSongs(contentResolver)
        handleSeekBarChanges(seekBar)
        startSongFromIntent(presenter!!.getSong(intent.getStringExtra("SongName")))
    }

    override fun onClick(v: View?) {
        when (v) {
            btnPlay -> {
                presenter?.play(presenter?.getSong())
                manageButtonsVisibility(v)
            }
            btnPause -> {
                presenter?.pause()
                manageButtonsVisibility(v)
            }
            btnNext -> {
                presenter?.next()
                manageButtonsVisibility(v)
            }
            btnPrevious -> {
                presenter?.previous()
                manageButtonsVisibility(v)
            }
            btnShuffle ->{
                presenter?.shuffle(presenter?.getSong(), contentResolver)
            }
            btnBack ->{
                onDestroy()
            }
        }
    }

    private fun handleSeekBarChanges(seekBar: SeekBar) {
        seekBarUserSubscription = RxSeekBar.changeEvents(seekBar)
                .ofType(SeekBarProgressChangeEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { seekBarChangeEvent: SeekBarProgressChangeEvent ->
                    currentPlayTime.text = presenter?.songProgressFormat(seekBarChangeEvent.progress())
                    if (seekBarChangeEvent.fromUser()) {
                        presenter?.handleSeekBarStatusChanges(seekBarChangeEvent.progress())
                        seekBar.progress = seekBarChangeEvent.progress()
                    }
                }
    }

    private fun manageButtonsVisibility(v: View?) {
        if (v == btnPause) {
            btnPlay.visibility = View.VISIBLE
            btnPause.visibility = View.GONE
        } else if (v == btnPlay || v == btnNext || v == btnPrevious) {
            btnPlay.visibility = View.GONE
            btnPause.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        if(!seekBarUserSubscription.isUnsubscribed){
        seekBarUserSubscription.unsubscribe()}
        super.onDestroy()
    }

    override fun manageStartSong(duration: Int, progressText: String) {
        seekBar.max = duration
        maxPlayTime.text = progressText
    }

    override fun handlePlayerStop() {
        seekBar.progress = 0
        currentPlayTime.text = resources.getString(R.string.starttime)
    }

    override fun handleStartSeekBarProgress() {
        presenter?.startProgress()
    }

    override fun setSongName() {
        songName.text = presenter?.getCurrentSongName()
    }

    override fun updateSeekBarPosition(currentProgress: Int) {
        seekBar.progress = currentProgress
    }

    private fun startSongFromIntent(song: Songs?){
        presenter?.stop()
        presenter?.play(song)
        manageButtonsVisibility(btnPlay)
    }
}