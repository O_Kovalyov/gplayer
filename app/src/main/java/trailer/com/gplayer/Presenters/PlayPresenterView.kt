package trailer.com.gplayer.Presenters

import android.content.ContentResolver
import android.widget.SeekBar
import android.widget.TextView
import trailer.com.gplayer.Model.Songs

interface PlayPresenterView {
    fun play(song: Songs?)
    fun pause()
    fun previous()
    fun next()
    fun shuffle(song: Songs?, contentResolver: ContentResolver)
}
