package trailer.com.gplayer.Presenters

interface PlayerView : SmallPlayerView{
        fun handleStartSeekBarProgress()
        fun handlePlayerStop()
        fun manageStartSong(duration: Int, progressText: String)
        fun updateSeekBarPosition(currentProgress: Int)
}