package trailer.com.gplayer.Presenters

import android.content.ContentResolver
import android.media.MediaPlayer
import android.os.Handler
import android.util.Log
import trailer.com.gplayer.External.External
import trailer.com.gplayer.Model.Songs
import java.io.IOException
import java.util.*

class PlayActivityPresenter(private var mPlayer: MediaPlayer, private var view: PlayerView) : PlayPresenterView {

    private var audioList: ArrayList<Songs>? = null
    private var currentPosition: Int? = null
    private var currentSong: Songs? = null
    private lateinit var handler : Handler
    private lateinit var runnable: Runnable
    private var isShuffled: Boolean = false

    override fun play(song: Songs?) {
        try {
            if (!mPlayer.isPlaying) {
                if (currentPosition == null) {
                    mPlayer.setDataSource(song!!.data)
                    mPlayer.prepare()
                    mPlayer.setOnPreparedListener { mp: MediaPlayer? ->
                        mp?.start()
                        view.handleStartSeekBarProgress()
                        currentSong = song
                        view.setSongName()
                        view.manageStartSong(currentSong!!.duration.toInt(),songProgressFormat(currentSong!!.duration.toInt()))
                    }
                } else if (currentPosition != null) {
                    mPlayer.seekTo(currentPosition!!)
                    mPlayer.start()
                    view.handleStartSeekBarProgress()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun pause() {
        try {
            if (mPlayer.isPlaying) {
                mPlayer.pause()
                currentPosition = mPlayer.currentPosition
            }
        } catch (e: Exception) {
            Log.e("PauseException", e.message)
        }

    }

    override fun previous() {
        try {
            if (mPlayer.isPlaying) {
                mPlayer.stop()
            }
                mPlayer.reset()
                if ((audioList!!.indexOf(currentSong) - 1) < 0) {
                    currentSong = audioList!![audioList!!.size - 1]
                    view.setSongName()
                } else {
                    currentSong = audioList!![(audioList!!.indexOf(currentSong) - 1)]
                    view.setSongName()
                }
                mPlayer.setDataSource(currentSong!!.data)
                mPlayer.prepare()
                mPlayer.setOnPreparedListener { mp: MediaPlayer? ->
                    mp?.start()
                    view.handleStartSeekBarProgress()
                    view.manageStartSong(currentSong!!.duration.toInt(),songProgressFormat(currentSong!!.duration.toInt()))
                }
            }catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun next() {
        try {
            if (mPlayer.isPlaying) {
                mPlayer.stop()
            }
                mPlayer.reset()
                if ((audioList!!.indexOf(currentSong) + 1) <= (audioList!!.size - 1)) {
                    currentSong = audioList!![(audioList!!.indexOf(currentSong) + 1)]
                    view.setSongName()
                } else {
                    currentSong = audioList!![0]
                    view.setSongName()
                }
                mPlayer.setDataSource(currentSong!!.data)
                mPlayer.prepare()
                mPlayer.setOnPreparedListener { mp: MediaPlayer? ->
                    mp?.start()
                    view.handleStartSeekBarProgress()
                    view.manageStartSong(currentSong!!.duration.toInt(),songProgressFormat(currentSong!!.duration.toInt()))
                }
            }  catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun shuffle(song: Songs?, contentResolver: ContentResolver) {
        if(isShuffled){
            audioList!!.shuffle()
            currentSong = song
        }else{
        audioList = External().getSongs(contentResolver)
            currentSong = song
        }
    }

    fun stop(){
        if(mPlayer.isPlaying){
        mPlayer.stop()
        }
        mPlayer.reset()
    }

    fun getStoredSongs(contentResolver: ContentResolver) {
        audioList = External().getSongs(contentResolver)
        currentSong = audioList!![Random().nextInt(audioList!!.size)]
    }

    fun getSong(): Songs? = audioList!![Random().nextInt(audioList!!.size)]

    fun getSong(song: String) : Songs{
        for(songs in audioList!!){
            if(songs.title==(song))
                return songs
        }
        return currentSong!!
    }

    fun getCurrentSongName():String? = "${currentSong!!.artist} ${currentSong!!.title}"

    fun songProgressFormat(progress: Int): String{
        val seconds = if(progress / 1000 % 60 < 10)
            "0${progress / 1000 % 60}"
        else
            "${progress / 1000 % 60}"
        return "${progress/1000/60}:$seconds"
    }

    fun handleSeekBarStatusChanges(progress: Int){
        mPlayer.seekTo(progress)
        currentPosition = progress
    }


    fun startProgress() {
         runnable = object : Runnable {
            override fun run() {
                if (mPlayer.isPlaying) {
                    handler.postDelayed(this, 1000)
                    view.updateSeekBarPosition(mPlayer.currentPosition)
                }
            }
        }
        handler = Handler()
        if (mPlayer.isPlaying) {
            handler.postDelayed(runnable, 1000)
        }
    }
}
