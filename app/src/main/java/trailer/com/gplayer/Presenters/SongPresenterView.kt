package trailer.com.gplayer.Presenters

import trailer.com.gplayer.Model.Songs

interface SongPresenterView {
    fun play()
    fun pause()
    fun next()
    fun filter(t: String, songList: List<Songs>): List<Songs>
    fun getCurrentSongName():String?
    fun getCurrentSongArtist():String?
}