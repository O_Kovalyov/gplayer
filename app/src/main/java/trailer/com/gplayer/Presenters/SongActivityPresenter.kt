package trailer.com.gplayer.Presenters

import android.media.MediaPlayer
import android.util.Log
import trailer.com.gplayer.Model.Songs
import java.io.IOException

class SongActivityPresenter(private var mPlayer: MediaPlayer, private var view: SmallPlayerView, private var audioList: java.util.ArrayList<Songs>) : SongPresenterView {

    private var currentPosition: Int? = null
    private var currentSong: Songs? = null
    private var isShuffled: Boolean = false


    override fun filter(t: String, songList: List<Songs>): List<Songs> {
        val filteredList: ArrayList<Songs> = ArrayList()
        for(song in songList){
            if (song.title!!.toLowerCase().contains(t.toLowerCase())||song.artist!!.toLowerCase().contains(t.toLowerCase())){
                filteredList.add(song)
            }
        }
        return filteredList
    }

    override fun play() {
        try {
            if (!mPlayer.isPlaying) {
                if (currentPosition == null) {
                    mPlayer.setDataSource(currentSong!!.data)
                    mPlayer.prepare()
                    mPlayer.setOnPreparedListener { mp: MediaPlayer? ->
                        mp?.start()
                        view.setSongName()
                    }
                } else if (currentPosition != null) {
                    mPlayer.seekTo(currentPosition!!)
                    mPlayer.start()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun pause() {
        try {
            if (mPlayer.isPlaying) {
                mPlayer.pause()
                currentPosition = mPlayer.currentPosition
            }
        } catch (e: Exception) {
            Log.e("PauseException", e.message)
        }
    }

    override fun next() {
        try {
            if (mPlayer.isPlaying) {
                mPlayer.stop()
            }
            mPlayer.reset()
            if ((audioList.indexOf(currentSong) + 1) <= (audioList.size - 1)) {
                currentSong = audioList[(audioList.indexOf(currentSong) + 1)]
                view.setSongName()
            } else {
                currentSong = audioList[0]
                view.setSongName()
            }
            mPlayer.setDataSource(currentSong!!.data)
            mPlayer.prepare()
            mPlayer.setOnPreparedListener { mp: MediaPlayer? ->
                mp?.start()
            }
        }  catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun setCurrentSong(song: Songs){
        currentSong = song
    }

    override fun getCurrentSongName():String? = currentSong!!.title
    override fun getCurrentSongArtist():String? = currentSong!!.artist

}