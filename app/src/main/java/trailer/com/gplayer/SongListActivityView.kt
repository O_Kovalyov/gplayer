package trailer.com.gplayer

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import kotlinx.android.synthetic.main.song_list.*
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import trailer.com.gplayer.Adapters.SongListRecyclerViewAdapter
import trailer.com.gplayer.Comparator.SongComparator
import trailer.com.gplayer.External.External
import trailer.com.gplayer.Model.Songs
import trailer.com.gplayer.PlayActivityElems.MediaPlayerSingleton
import trailer.com.gplayer.Presenters.SmallPlayerView
import trailer.com.gplayer.Presenters.SongActivityPresenter
import java.util.concurrent.TimeUnit

class SongListActivityView : Activity(), View.OnClickListener, SmallPlayerView {

    private lateinit var songListAdapter: SongListRecyclerViewAdapter
    private lateinit var searchViewSubscription: rx.Subscription
    private lateinit var searchViewExpandSubscription: Subscription
    private val rxSearchObservable = trailer.com.gplayer.RxObservables.RxSearchObservable()
    private var subscriptionList: ArrayList<Subscription?> = ArrayList()
    private lateinit var songActivityPresenter: SongActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.song_list)
        smallPlayerContainer.visibility = View.GONE
        val editText: EditText = searchSongBtn.findViewById(android.support.v7.appcompat.R.id.search_src_text)
        editText.setTextColor(Color.WHITE)
        editText.setHint(R.string.search_hint)
        editText.setHintTextColor(Color.GRAY)
        songList.layoutManager = LinearLayoutManager(this)
        songListAdapter = SongListRecyclerViewAdapter()
        songList.adapter = songListAdapter
        songActivityPresenter = SongActivityPresenter(MediaPlayerSingleton.instance.getMPlayer(),
                this, External().getSongs(contentResolver))
        songListAdapter.songList = External().getSongs(contentResolver)
        songListAdapter.sortContent(SongComparator().getTitleComparator())
        handleRecyclerViewClick()
        handleSearchViewChanges(searchSongBtn)
        handleCloseViewChanges(searchSongBtn,songListBackButton)
        smallPlayerBtnPause.setOnClickListener(this)
        smallPlayerBtnPlay.setOnClickListener(this)
        smallPlayerForward.setOnClickListener(this)
    }

    private fun handleSearchViewChanges(searchView: android.support.v7.widget.SearchView) {
        searchViewSubscription = rxSearchObservable.queryChangeHandlerFromView(searchView)
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribeOn(rx.schedulers.Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { t ->
                    songListAdapter.songList = songActivityPresenter.filter(t, External()
                            .getSongs(contentResolver))
                    songListAdapter.sortContent(SongComparator().getTitleComparator())
                }
        subscriptionList.add(searchViewSubscription)
    }

    private fun handleCloseViewChanges(searchView: android.support.v7.widget.SearchView, viewToHide: ImageButton) {
        searchViewExpandSubscription = rxSearchObservable.closeHandlerFromView(searchView, viewToHide)
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribeOn(rx.schedulers.Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {}
        subscriptionList.add(searchViewExpandSubscription)
    }


    private fun handleRecyclerViewClick() {
        val subscription: Subscription? = songListAdapter.clickEvent
                .subscribe { it ->
                    songActivityPresenter.setCurrentSong(it)
                    handleSmallPlayerStart(it)
                    val intent = Intent(this, PlayActivityView::class.java)
                    intent.putExtra("SongName", it.title)
                    startActivity(intent)
                }
        subscriptionList.add(subscription)
    }

    override fun onClick(v: View?) {
        when(v){
            smallPlayerBtnPlay ->{
                songActivityPresenter.play()
                manageButtonsVisibility(v)
            }
            smallPlayerBtnPause ->{
                songActivityPresenter.pause()
                manageButtonsVisibility(v)
            }
            smallPlayerForward -> {
                songActivityPresenter.next()
                manageButtonsVisibility(v)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        for (subscription in subscriptionList) {
            subscription!!.unsubscribe()
        }
    }

    override fun setSongName() {
        smallPlayerTitle.text = songActivityPresenter.getCurrentSongName()
        smallPlayerArtist.text = songActivityPresenter.getCurrentSongArtist()
    }

    private fun handleSmallPlayerStart(song: Songs){
        smallPlayerContainer.visibility = View.VISIBLE
        smallPlayerTitle.text = "${song.title}"
        smallPlayerArtist.text ="${song.artist}"
        smallPlayerPosterLogo.setImageDrawable(resources.getDrawable(R.drawable.background_blur))
        manageButtonsVisibility(smallPlayerBtnPlay)
    }

    private fun manageButtonsVisibility(v: View?) {
        if (v == smallPlayerBtnPause) {
            smallPlayerBtnPlay.visibility = View.VISIBLE
            smallPlayerBtnPause.visibility = View.GONE
        } else if (v == smallPlayerBtnPlay || v == smallPlayerForward) {
            smallPlayerBtnPlay.visibility = View.GONE
            smallPlayerBtnPause.visibility = View.VISIBLE
        }
    }
}
